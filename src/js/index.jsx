import React from 'react';
import { Component } from 'react';
import { render } from 'react-dom';

import Landing from '../components/Landing';
import '../scss/main.scss';

class Application extends Component {
    render() {
        return (
            <Landing />
        )
    }
};
render(<Application />, document.getElementById('app'));

export default Application;
