import React from 'react';
import { Component } from 'react';

const SearchIcon = () => {
    return (
        <React.Fragment>
            <svg xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 40 40'
                width='40'
                height='40'
                className="icon"
            >
                <line x1={5} x2={15} y1={35} y2={25} strokeWidth='1.5' stroke='white' />
                <circle cx={25} cy={15} r={10} strokeWidth='1.5' stroke='white' fill='none' />
            </svg>
        </React.Fragment>
    );
};

export default SearchIcon;
