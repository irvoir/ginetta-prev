import React from 'react';
import { Component } from 'react';
import PropTypes from 'prop-types';
import SearchIcon from './Icons';
import Text from './Text';

const KEY = `87e4ca133aa307e9cdd306dc7465c72881dd547f`;

/**
 * HOC for results to wrap around 'results' className
 */
const Results = (props, key) => Result => {
    return(
        <div className="results">
            {key !== undefined ? <Result props={Object.assign(props, { key: key })} /> : <Result {...props} /> }
        </div>
    );
};

/**
 * Result component, used to render results (max of 5) or an error message.
 */
class Result extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
        // Destructure the object to flatten the object
        let { props } = this.props;
        this.setState({
            props,
            active: 0
        });
    }
    static getDerivedStateFromProps({ props }, state) {
        // Map a let statement for each keyCode received from Landing Component
        let arrowUp = 38,
            arrowDown = 40,
            enter = 13,
            // Double check that it exists so it does not throw an error
            key = props && props.key || '';
        // Return a new state to apply a specific className to result span
        return key === arrowUp ? { active: state.active === 0 ? 0 : state.active - 1, goToWebsite: false } :
            key === arrowDown ? { active: state.active === 4 ? 4 : state.active + 1, goToWebsite: false } :
            key === enter ? { goToWebsite: true, active: state.active } : { ...props, goToWebsite: false };
    }
    render() {
        // Map each result and then render it if props.error is false
        return (this.state && this.state.res && !this.props.err ?
            // Limit the array, and map only the first 5 elements from it
            Object.values(this.state.res).slice(0, 5)
                .sort()
                .map((element, index) => {
                    // Navigate to the html_url if the user presses 'Enter' key
                    if (this.state.goToWebsite) window.location.href = this.state.urls[this.state.active];
                    let term = this.state.term.toLowerCase();
                    let replaced = element.toLowerCase().replace(term, '');
                    return (
                        // Apply 'highlighted' class for the first element, then decide based on arrow keys
                        <div className={this.state.active === index ? 'result active' : 'result'} key={index}>
                            {/* Click on result span to navigate to users GitHub account */}
                            <a className="link" href={this.state.urls[index]} />
                            <img className="img" src={this.state.img[index]} />
                            <span className="span-item">
                                {/* Separate the current search string from the rest of the result string, for highliting purposes */}
                                <strong>
                                    <Text text={term} />
                                </strong>
                                {replaced.length > 0 ? <span style={{color: `#777`}}>
                                    {replaced}
                                </span> : null}
                            </span>
                        </div>
                    )
                    {/* Render an error if API returns > 200 */}
                }) : <div className="results">
                    <span className="result span-item">
                        <Text text={'Error!'} />
                    </span>
                </div>
        );
    }
}

class Input extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            // Glue an icon with the input
            <div className="input-area">
                <SearchIcon />
                {this.props.children}
            </div>
        );
    }
}
/**
 * Main application component where most of the logic resides
 */
export default class Landing extends Component {
    constructor(props) {
        super(props);
        this.state = { fetched: false };
        this.handleTextInput = this.handleTextInput.bind(this);
        this.focusTextInput = this.focusTextInput.bind(this);
        this.keyPressed = this.keyPressed.bind(this);
        this.ref = React.createRef();
    }
    componentDidMount() {
        this.focusTextInput();
    }
    focusTextInput() {
        // Not needed but focus the input when the component is mounted
        this.ref.current.focus();
    }
    keyPressed(event) {
        // Only change the state if arrow up, down and enter key are pressed
        if (event.keyCode === 40 || event.keyCode === 38 || event.keyCode === 13) {
            this.setState({ keyPressed: event.keyCode })
        } else {
            this.setState({ keyPressed: '' })
        }
    }
    handleTextInput(event) {
        let searchValue = event.target.value;
        // Limit the search to 3 or more words
        if (searchValue.length > 3 && !this.state.fetched) {
            // Use native fetch API for the get request
            fetch(`https://api.github.com/search/users?q=${searchValue}&acess_token=${KEY}`)
                // return an JSON parsed object if the status is == 200 else it's an error
                .then(res => res.status == 200 ? res.json() : this.setState({ error: true, fetched: true }))
                .then(
                    res => {
                        if (res == undefined) this.setState({ results: '', error: true, fetched: true });
                        else {
                            let logins = [], avatars = [], urls = [];
                            // Loop through items that the API returned
                            for (let v of res.items) {
                                // Remove unwanted login names that don't start with the searchValue
                                if (!v.login.toLowerCase().startsWith(searchValue.toLowerCase())) continue;
                                logins.push(v.login);
                                avatars.push(v.avatar_url);
                                urls.push(v.html_url);
                            }
                            // Send a new state to display the results
                            this.setState({ results: {
                                res: logins,
                                img: avatars,
                                urls: urls,
                                term: searchValue
                            }});
                        };
                    },
                );
        } else this.setState({ results: '' });
    }
    render() {
        return (
            // Listen for keyDown events, also focus the whole div/body so we can take those events too
            <div className="landing" onKeyDown={this.keyPressed} tabIndex="0">
                {/* Use a 'slot' like children to send the input */}
                <Input children={
                    <input className="search"
                        onChange={this.handleTextInput}
                        onClick={this.focusTextInput}
                        ref={this.ref}
                    />}
                />
                {(this.state && this.state.results && !this.state.error) ?
                        Results(this.state.results, this.state.keyPressed || '')(Result) :
                        this.state.error ? <Result err={this.state.error} /> : null
                }
            </div>
        );
    }
}
