const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin    = require('html-webpack-plugin');
const webpack              = require('webpack');
const path                 = require('path');
const devMode = process.env.NODE_ENV !== 'production';

const paths = {
    DIST : path.resolve(__dirname, 'dist'),
    SRC  : path.resolve(__dirname, 'src'),
    JS   : path.resolve(__dirname, 'src/js'),
};

module.exports = {
    mode: 'development',
    entry: path.join(paths.JS, 'index.jsx'),
    output: {
        path: paths.DIST,
        filename: 'app.bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [
                    'babel-loader',
                ]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "sass-loader"
                }]
            }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(paths.SRC, 'index.html'),
        }),
        new webpack.HotModuleReplacementPlugin(),
        new MiniCssExtractPlugin({
            filename: devMode ? '[name].css' : '[name].[hash].css',
            chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
        })
    ],
    resolve: {
        extensions: ['.js', '.jsx']
    },
    devServer: {
        stats: "errors-only",
        host: process.env.HOST, // Defaults to `localhost`
        port: process.env.PORT, // Defaults to 8080
        overlay: true,
        hot: false
    }
};
